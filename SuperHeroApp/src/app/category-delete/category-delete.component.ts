import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Category } from '../models/category.model';
import { CategoryService } from '../providers/category.service';

@Component({
  selector: 'app-category-delete',
  templateUrl: './category-delete.component.html',
  styleUrls: ['./category-delete.component.css']
})
export class CategoryDeleteComponent {
  category!: Category;
  id: number = 0;
  name: string = "";
  description: string = "";

  constructor(private activatedRoute: ActivatedRoute, private CategoryService: CategoryService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']
    });
    this.CategoryService.getCategoryById(this.id).subscribe((data) => {
      this.category = data;
      this.name = this.category.name;
      this.description = this.category.description;
    });
  }
  onClickDeleteButton() {
    this.CategoryService.deleteCategory(this.category).subscribe(data => console.dir(data));
    alert("Record deleted");
    this.router.navigate(['/categories']);
  }
}
