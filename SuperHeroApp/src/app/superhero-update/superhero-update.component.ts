import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Category } from '../models/category.model';
import { Superhero } from '../models/superhero.model';
import { Superpower } from '../models/superpower.model';
import { SuperheroService } from '../providers/superhero.service';
import { SuperpowerService } from '../providers/superpower.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-superhero-update',
  templateUrl: './superhero-update.component.html',
  styleUrls: ['./superhero-update.component.css']
})
export class SuperheroUpdateComponent {
  superhero!: Superhero;
  superheroId: number = 0;
  name: string = "";
  nickname: string = "";
  telephoneNumber: string = "";
  dateOfBirth: string = "";
  superpowers: Array<Superpower> = [];
  selectedSuperpowers: number[] = []; // array to store the IDs of selected superpowers
  superpower!: Superpower;
  weakness?: Category = new Category(2, "Ice", "Superheroes with ice-based abilities");

  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService, private SuperpowerService: SuperpowerService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.superheroId = params['id']
    });
    this.SuperheroService.getSuperheroById(this.superheroId).subscribe((data) => {
      this.superhero = data;
      this.name = data.name;
      this.nickname = data.nickname;
      this.telephoneNumber = data.telephoneNumber;
      this.superhero.dateOfBirth = data.dateOfBirth.split("T")[0];
      if (data.superpowers != null){
      this.selectedSuperpowers = data.superpowers.map(superpower => superpower.id); // Set the initial values of the checkboxes
      }
    });

    this.SuperpowerService.getSuperpowers().subscribe((data) => {
      this.superpowers = data;
      console.dir(this.superpowers);
    });
  }
  onClickSaveButton() {
    const selectedSuperpowers = this.superpowers
      .filter(superpower => this.selectedSuperpowers.includes(superpower.id))
      .map(superpower => ({ id: superpower.id, name: superpower.name, description: superpower.description }));
    // const superhero = new Superhero(
    //   0,
    //   this.name,
    //   this.nickname,
    //   this.telephoneNumber,
    //   this.dateOfBirth,
    //   0,
    //   0,
    //   this.weakness,
    //   selectedSuperpowers
    // );
    this.superhero.superpowers = selectedSuperpowers;
    console.dir(this.superhero);
    this.SuperheroService.updateSuperhero(this.superhero).subscribe(data => console.dir(data));
    alert("Record updated");
    this.router.navigate(['/superheroes']);
  }
  onChangeSuperpower(event: any, superpower: Superpower) {
    if (event.target.checked) {
      this.selectedSuperpowers.push(superpower.id);
    } else {
      const index = this.selectedSuperpowers.indexOf(superpower.id);
      if (index > -1) {
        this.selectedSuperpowers.splice(index, 1);
      }
    }
  }
}
