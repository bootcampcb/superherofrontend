import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperheroUpdateComponent } from './superhero-update.component';

describe('SuperheroUpdateComponent', () => {
  let component: SuperheroUpdateComponent;
  let fixture: ComponentFixture<SuperheroUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperheroUpdateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuperheroUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
