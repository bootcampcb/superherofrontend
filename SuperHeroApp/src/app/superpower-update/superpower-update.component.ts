import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Superpower } from '../models/superpower.model';
import { SuperpowerService } from '../providers/superpower.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-superpower-update',
  templateUrl: './superpower-update.component.html',
  styleUrls: ['./superpower-update.component.css']
})
export class SuperpowerUpdateComponent {
  superpower!: Superpower;
  superpowerId: number = 0;
  name: string = "";
  description: string = "";

  constructor(private activatedRoute: ActivatedRoute, private SuperpowerService: SuperpowerService, private router: Router) {

  }
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.superpowerId = params['id']});
    this.SuperpowerService.getSuperpowerById(this.superpowerId).subscribe((data) => {
      this.superpower = data;
      this.name = data.name;
      this.description = data.description;
    });
  }
  onClickSaveButton() {
    this.SuperpowerService.updateSuperpower(new Superpower(this.superpowerId, this.name, this.description)).subscribe(data => console.dir(data));
    alert("Record updated");
    this.router.navigate(['/superpowers']);
  }
}
