import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperpowerUpdateComponent } from './superpower-update.component';

describe('SuperpowerUpdateComponent', () => {
  let component: SuperpowerUpdateComponent;
  let fixture: ComponentFixture<SuperpowerUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperpowerUpdateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuperpowerUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
