import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';

@Component({
  selector: 'app-superhero',
  templateUrl: './superhero.component.html',
  styleUrls: ['./superhero.component.css']
})
export class SuperheroComponent {
  superheroes: Array<Superhero> = [];
  powerId: number = 0;
  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService) { }

  ngOnInit(): void {
    this.SuperheroService.getSuperheroes().subscribe((data) => {
      this.superheroes = data;
    });
  }
  onClickSearchButton() {
    this.SuperheroService.getSuperheroesByPower(this.powerId).subscribe((data) => {
      this.superheroes = data;
    });
  }
}
