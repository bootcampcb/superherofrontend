import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Superpower } from '../models/superpower.model';
import { SuperpowerService } from '../providers/superpower.service';

@Component({
  selector: 'app-superpower-delete',
  templateUrl: './superpower-delete.component.html',
  styleUrls: ['./superpower-delete.component.css']
})
export class SuperpowerDeleteComponent {
  superpower!: Superpower;
  id: number = 0;
  name: string = "";
  description: string = "";

  constructor(private activatedRoute: ActivatedRoute, private SuperpowerService: SuperpowerService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']
    });
    this.SuperpowerService.getSuperpowerById(this.id).subscribe((data) => {
      this.superpower = data;
      this.name = this.superpower.name;
      this.description = this.superpower.description;
    });
  }
  onClickDeleteButton() {
    this.SuperpowerService.deleteSuperpower(this.superpower).subscribe(data => console.dir(data));
    alert("Record deleted");
    this.router.navigate(['/superpowers']);
  }
}
