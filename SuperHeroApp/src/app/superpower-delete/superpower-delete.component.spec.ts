import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperpowerDeleteComponent } from './superpower-delete.component';

describe('SuperpowerDeleteComponent', () => {
  let component: SuperpowerDeleteComponent;
  let fixture: ComponentFixture<SuperpowerDeleteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperpowerDeleteComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuperpowerDeleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
