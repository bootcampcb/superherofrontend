import { Superpower } from "./superpower.model";

export class Category {
    constructor(public id: number, public name: string, public description: string, public superpowers?: Array<Superpower>) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.superpowers = superpowers;
    }
}
