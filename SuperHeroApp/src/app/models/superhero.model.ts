import { Category } from "./category.model";
import { Superpower } from "./superpower.model";

export class Superhero {
    constructor (
        public id: number,
        public name: string,
        public nickname: string,
        public telephoneNumber: string,
        public dateOfBirth: string,
        public superpowerId?: number,
        public weaknessId?: number,
        public weakness?: Category,
        public superpowers?: Array<Superpower>
    ) {
        this.id = id;
        this.name = name;
        this.nickname = nickname;
        this.telephoneNumber = telephoneNumber;
        this.dateOfBirth = dateOfBirth;
        this.superpowerId = superpowerId;
        this.weaknessId = weaknessId;
        // this.weakness = weakness();
        this.superpowers = superpowers;
    }
}

