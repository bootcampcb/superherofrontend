import { Component } from '@angular/core';
import { Category } from '../models/category.model';
import { Superpower } from '../models/superpower.model';
import { SuperpowerService } from '../providers/superpower.service';
import { CategoryService } from '../providers/category.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-category-add',
  templateUrl: './category-add.component.html',
  styleUrls: ['./category-add.component.css']
})
export class CategoryAddComponent {
  superpowers!: Superpower;
  name: string = "";
  description: string = "";

  constructor(private activatedRoute: ActivatedRoute, private CategoryService: CategoryService, private router: Router) {
  }

  onClickAddButton() {
    this.CategoryService.postNewCategory(new Category(0, this.name, this.description)).subscribe(data => console.dir(data));
    this.router.navigate(['/categories']);
  }
}
