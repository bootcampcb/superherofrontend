import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-superhero-details',
  templateUrl: './superhero-details.component.html',
  styleUrls: ['./superhero-details.component.css']
})
export class SuperheroDetailsComponent {
  superhero!: Superhero;
  superheroId: number = 0;

  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService, private router: Router) { }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.superheroId = params['id']});
    this.SuperheroService.getSuperheroById(this.superheroId).subscribe((data) => {
      this.superhero = data;
//      alert(this.superhero.name);
      console.dir(this.superhero);
    });
  }
  
  onClickUpdateButton() {
    this.router.navigate(['/superhero-update'], {queryParams: {id: this.superheroId}});
  }

}
