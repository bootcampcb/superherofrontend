import { Component } from '@angular/core';
import { Category } from '../models/category.model';
import { Superpower } from '../models/superpower.model';
import { Superhero } from '../models/superhero.model';
import { SuperpowerService } from '../providers/superpower.service';
import { SuperheroService } from '../providers/superhero.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-superhero-add',
  templateUrl: './superhero-add.component.html',
  styleUrls: ['./superhero-add.component.css']
})
export class SuperheroAddComponent {
  name: string = "";
  nickname: string = "";
  telephoneNumber: string = "";
  dateOfBirth: string = "";
  superpowers: Array<Superpower> = [];
  selectedSuperpowers: number[] = []; // array to store the IDs of selected superpowers
  superpower!: Superpower;
  weakness?: Category; // = new Category(2, "Ice", "Superheroes with ice-based abilities");

  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService, private SuperpowerService: SuperpowerService, private router: Router) {
  }

  ngOnInit(): void {
    this.SuperpowerService.getSuperpowers().subscribe((data) => {
      this.superpowers = data;
      console.dir(this.superpowers);
    });
  }

  onClickAddButton() {
    const selectedSuperpowers = this.superpowers
      .filter(superpower => this.selectedSuperpowers.includes(superpower.id))
      .map(superpower => ({id: superpower.id, name: superpower.name, description: superpower.description}));
    const superhero = new Superhero(
      0,
      this.name,
      this.nickname,
      this.telephoneNumber,
      this.dateOfBirth,
      0,
      2,
      this.weakness,
      selectedSuperpowers
    );
    console.dir(superhero);
    this.SuperheroService.postNewSuperhero(superhero).subscribe(data => console.dir(data));
    this.router.navigate(['/superheroes']);
  }

  onChangeSuperpower(event: any, superpower: Superpower) {
    if (event.target.checked) {
      this.selectedSuperpowers.push(superpower.id);
    } else {
      const index = this.selectedSuperpowers.indexOf(superpower.id);
      if (index > -1) {
        this.selectedSuperpowers.splice(index, 1);
      }
    }
  }
}