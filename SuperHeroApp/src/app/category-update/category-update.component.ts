import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Category } from '../models/category.model';
import { CategoryService } from '../providers/category.service';
import { Router } from '@angular/router';
import { Superpower } from '../models/superpower.model';
import { SuperpowerService } from '../providers/superpower.service';

@Component({
  selector: 'app-category-update',
  templateUrl: './category-update.component.html',
  styleUrls: ['./category-update.component.css']
})
export class CategoryUpdateComponent {
  category!: Category;
  categoryId: number = 0;
  name: string = "";
  description: string = "";
  superpowers: Array<Superpower> = [];
  selectedSuperpowers: number[] = []; // array to store the IDs of selected superpowers
  superpower!: Superpower;

  constructor(private activatedRoute: ActivatedRoute, private CategoryService: CategoryService, private SuperpowerService: SuperpowerService, private router: Router) {

  }
  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.categoryId = params['id']});
    this.CategoryService.getCategoryById(this.categoryId).subscribe((data) => {
      this.category = data;
      this.name = data.name;
      this.description = data.description;
      if (data.superpowers != null){
        this.selectedSuperpowers = data.superpowers.map(superpower => superpower.id); // Set the initial values of the checkboxes
        }
    });
    this.SuperpowerService.getSuperpowers().subscribe((data) => {
      this.superpowers = data;
      console.dir(this.superpowers);
    });
  }
  // onClickSaveButton() {
  //   this.CategoryService.updateCategory(new Category(this.categoryId, this.name, this.description)).subscribe(data => console.dir(data));
  //   alert("Record updated");
  //   this.router.navigate(['/categories']);
  // }
  onClickSaveButton() {
    const selectedSuperpowers = this.superpowers
      .filter(superpower => this.selectedSuperpowers.includes(superpower.id))
      .map(superpower => ({ id: superpower.id, name: superpower.name, description: superpower.description }));

    this.category.superpowers = selectedSuperpowers;
    console.dir(this.category);
    this.CategoryService.updateCategory(this.category).subscribe(data => console.dir(data));
    alert("Record updated");
    this.router.navigate(['/categories']);
  }
  onChangeSuperpower(event: any, superpower: Superpower) {
    if (event.target.checked) {
      this.selectedSuperpowers.push(superpower.id);
    } else {
      const index = this.selectedSuperpowers.indexOf(superpower.id);
      if (index > -1) {
        this.selectedSuperpowers.splice(index, 1);
      }
    }
  }
}
