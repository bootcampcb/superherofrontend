import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Category } from '../models/category.model';
import { CategoryService } from '../providers/category.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent {
  categories: Array<Category> = [];
  categoryId!: number;
  constructor(private activatedRoute: ActivatedRoute, private CategoryService: CategoryService) { }

  ngOnInit(): void {
    this.CategoryService.getCategories().subscribe((data) => {
      this.categories = data;
      console.dir(this.categories);
    });
  }
}
