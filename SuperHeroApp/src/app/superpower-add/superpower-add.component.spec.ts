import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperpowerAddComponent } from './superpower-add.component';

describe('SuperpowerAddComponent', () => {
  let component: SuperpowerAddComponent;
  let fixture: ComponentFixture<SuperpowerAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperpowerAddComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuperpowerAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
