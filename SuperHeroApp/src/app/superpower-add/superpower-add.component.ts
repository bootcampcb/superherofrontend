import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Superpower } from '../models/superpower.model';
import { SuperpowerService } from '../providers/superpower.service';

@Component({
  selector: 'app-superpower-add',
  templateUrl: './superpower-add.component.html',
  styleUrls: ['./superpower-add.component.css']
})
export class SuperpowerAddComponent {
  superpowers!: Superpower;
  name: string = "";
  description: string = "";

  constructor(private activatedRoute: ActivatedRoute, private SuperpowerService: SuperpowerService, private router: Router) {
  }

  onClickAddButton() {
    this.SuperpowerService.postNewSuperpower(new Superpower(0, this.name, this.description)).subscribe(data => console.dir(data));
    this.router.navigate(['/superpowers']);
  }
}
