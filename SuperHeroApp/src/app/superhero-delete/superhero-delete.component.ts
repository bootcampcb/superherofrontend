import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';

@Component({
  selector: 'app-superhero-delete',
  templateUrl: './superhero-delete.component.html',
  styleUrls: ['./superhero-delete.component.css']
})
export class SuperheroDeleteComponent {
  superhero!: Superhero;
  id: number = 0;
  name: string = "";
  nickname: string = "";

  constructor(private activatedRoute: ActivatedRoute, private SuperheroService: SuperheroService, private router: Router) {
  }

  ngOnInit(): void {
    this.activatedRoute.queryParams.subscribe((params) => {
      this.id = params['id']});
      this.SuperheroService.getSuperheroById(this.id).subscribe((data) => {
        this.superhero = data;
        this.name = this.superhero.name;
        this.nickname = this.superhero.nickname;
      });
  }
  onClickDeleteButton() {
    this.SuperheroService.deleteSuperhero(this.superhero).subscribe(data => console.dir(data));
    alert("Record deleted");
    this.router.navigate(['/superheroes']);
  }
}
