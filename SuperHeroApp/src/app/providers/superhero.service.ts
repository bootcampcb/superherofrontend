import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Superhero } from '../models/superhero.model';

@Injectable({
  providedIn: 'root'
})
export class SuperheroService {
  url = "https://localhost:7043/api/";
  constructor(private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      // 'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };
  getSuperheroes(): Observable<Superhero[]> {
    return this.http
      .get(this.url + "Superheroes", this.httpOptions)
      .pipe(map((res) => <Superhero[]>res));
  }
  getLast3Superheroes(): Observable<Superhero[]> {
    return this.http
      .get(this.url + "Superheroes/Superheroes3", this.httpOptions)
      .pipe(map((res) => <Superhero[]>res));
  }
  getSuperheroesByPower(powerId: number): Observable<Superhero[]> {
    return this.http
      .get(this.url + "Superheroes/" + powerId +"/powerId", this.httpOptions)
      .pipe(map((res) => <Superhero[]>res));
  }
  getSuperheroById(id: number): Observable<Superhero> {
    return this.http
      .get(this.url + "Superheroes/" + id, this.httpOptions)
      .pipe(map((res) => <Superhero>res));
  }
  postNewSuperhero(Superhero: Superhero): Observable<Superhero> {
    return this.http.post<Superhero>(this.url + 'Superheroes',
      {

        name: Superhero.name,
        nickname: Superhero.nickname,
        telephoneNumber: Superhero.telephoneNumber,
        dateOfBirth: Superhero.dateOfBirth,
        superpowerId: Superhero.superpowerId,
        weaknessId: Superhero.weaknessId,
        weakness: Superhero.weakness,
        superpowers: Superhero.superpowers
      },
      this.httpOptions);
  }
  updateSuperhero(Superhero: Superhero): Observable<Superhero> {
    return this.http.put<Superhero>(this.url + 'Superheroes/' + Superhero.id,
      {
        id: Superhero.id,
        name: Superhero.name,
        nickname: Superhero.nickname,
        telephoneNumber: Superhero.telephoneNumber,
        dateOfBirth: Superhero.dateOfBirth,
        superpowerId: Superhero.superpowerId,
        weaknessId: Superhero.weaknessId,
        weakness: Superhero.weakness,
        superpowers: Superhero.superpowers
      },
      this.httpOptions);
  }
  deleteSuperhero(Superhero: Superhero): Observable<Superhero> {
    return this.http.delete<Superhero>(this.url + 'Superheroes/' + Superhero.id,
      this.httpOptions);
  }
}
