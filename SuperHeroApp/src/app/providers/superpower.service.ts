import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Superpower } from '../models/superpower.model';

@Injectable({
  providedIn: 'root'
})
export class SuperpowerService {
  url = "https://localhost:7043/api/";
  constructor(private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      // 'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };
  getSuperpowers(): Observable<Superpower[]> {
    return this.http
      .get(this.url + "Superpowers", this.httpOptions)
      .pipe(map((res) => <Superpower[]>res));
  }
  getSuperpowersByHero(heroId: number): Observable<Superpower[]> {
    return this.http
      .get(this.url + "Superpowers/" + heroId +"/heroId", this.httpOptions)
      .pipe(map((res) => <Superpower[]>res));
  }
  getSuperpowerById(id: number): Observable<Superpower> {
    return this.http
      .get(this.url + "Superpowers/" + id, this.httpOptions)
      .pipe(map((res) => <Superpower>res));
  }
  postNewSuperpower(Superpower: Superpower): Observable<Superpower> {
    console.dir(Superpower);
    return this.http.post<Superpower>(this.url + 'Superpowers',
      {
        id: Superpower.id,
        name: Superpower.name,
        description: Superpower.description
      },
      this.httpOptions);
  }
  updateSuperpower(Superpower: Superpower): Observable<Superpower> {
    return this.http.put<Superpower>(this.url + 'Superpowers/' + Superpower.id,
      {
        id: Superpower.id,
        name: Superpower.name,
        description: Superpower.description
      },
      this.httpOptions);
  }
  deleteSuperpower(Superpower: Superpower): Observable<Superpower> {
    return this.http.delete<Superpower>(this.url + 'Superpowers/' + Superpower.id,
      this.httpOptions);
  }
}
