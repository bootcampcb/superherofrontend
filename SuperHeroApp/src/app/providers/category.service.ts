import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Category } from '../models/category.model';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  url = "https://localhost:7043/api/";
  constructor(private http: HttpClient) { }
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      // 'Referrer-Policy': 'strict-origin-when-cross-origin',
    }),
  };
  getCategories(): Observable<Category[]> {
    return this.http
      .get(this.url + "Categories", this.httpOptions)
      .pipe(map((res) => <Category[]>res));
  }
  getCategoryById(id: number): Observable<Category> {
    return this.http
      .get(this.url + "Categories/" + id, this.httpOptions)
      .pipe(map((res) => <Category>res));
  }
  postNewCategory(Category: Category): Observable<Category> {
    console.dir(Category);
    return this.http.post<Category>(this.url + 'Categories',
      {
        id: Category.id,
        name: Category.name,
        description: Category.description
      },
      this.httpOptions);
  }
  updateCategory(Category: Category): Observable<Category> {
    return this.http.put<Category>(this.url + 'Categories/' + Category.id,
      {
        id: Category.id,
        name: Category.name,
        description: Category.description
      },
      this.httpOptions);
  }
  deleteCategory(Category: Category): Observable<Category> {
    return this.http.delete<Category>(this.url + 'Categories/' + Category.id,
      this.httpOptions);
  }
}
