import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperpowerDetailsComponent } from './superpower-details.component';

describe('SuperpowerDetailsComponent', () => {
  let component: SuperpowerDetailsComponent;
  let fixture: ComponentFixture<SuperpowerDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SuperpowerDetailsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SuperpowerDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
