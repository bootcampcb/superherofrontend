import { Component } from '@angular/core';
import { Superhero } from '../models/superhero.model';
import { SuperheroService } from '../providers/superhero.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  superheroes!: Array<Superhero>;
  intro: string = "In the year 3542, the world is a happy place thanks to the superheroes who have saved the planet from climate change and other world problems. But even in this perfect world, emergencies still occur, and we need all the superheroes we can get to help us when we need it the most.";
  details: string = "That's where the Superhero Registration System comes in. This voluntary sign-up system allows superheroes to register with us so we can call on them for help in times of crisis. By registering with us, superheroes can help ensure that the world remains a happy and peaceful place for generations to come.";
  conclusion: string = "If you have any questions or concerns about the Superhero Registration System, please don't hesitate to contact us using the information below.";
  picSrc: string = "";
  altTag: string = "";
  
  constructor(private superhereservice: SuperheroService) { }

  ngOnInit(): void {
    this.superhereservice.getLast3Superheroes().subscribe((data) => {
      this.superheroes = data});
  }
}
