import { Component } from '@angular/core';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.css']
})
export class ApplicationComponent {
  name: string = "";
  phone: string = "";
  qualifications: string = "";
  submitted: boolean = false;

  onSubmit(){
    this.submitted = true;
  }
}
