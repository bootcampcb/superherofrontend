import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SuperheroComponent } from './superhero/superhero.component';
import { SuperheroAddComponent } from './superhero-add/superhero-add.component';
import { SuperheroUpdateComponent } from './superhero-update/superhero-update.component';
import { SuperheroDeleteComponent } from './superhero-delete/superhero-delete.component';
import { SuperpowerComponent } from './superpower/superpower.component';
import { SuperpowerAddComponent } from './superpower-add/superpower-add.component';
import { SuperpowerUpdateComponent } from './superpower-update/superpower-update.component';
import { SuperpowerDeleteComponent } from './superpower-delete/superpower-delete.component';
import { SuperheroService } from './providers/superhero.service';
import { SuperpowerService } from './providers/superpower.service';
import { SuperpowerDetailsComponent } from './superpower-details/superpower-details.component';
import { SuperheroDetailsComponent } from './superhero-details/superhero-details.component';
import { ApplicationComponent } from './application/application.component';
import { CategoryComponent } from './category/category.component';
import { CategoryAddComponent } from './category-add/category-add.component';
import { CategoryDeleteComponent } from './category-delete/category-delete.component';
import { CategoryUpdateComponent } from './category-update/category-update.component';

const appRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: "home", title: 'Superhero Registry', component: HomeComponent },
  { path: "superheroes", title: 'Welcome to the Superhero Registration System', component: SuperheroComponent },
  { path: "superhero-details", title: 'Details', component: SuperheroDetailsComponent },
  { path: "superhero-add", title: 'Add Superhero', component: SuperheroAddComponent },
  { path: "superhero-delete", title: 'Confirm Delete', component: SuperheroDeleteComponent },
  { path: "superhero-update", title: 'Update Superhero', component: SuperheroUpdateComponent},
  { path: "application", title: 'Superhero registry Application', component: ApplicationComponent},
  { path: "superpowers", title: 'Superpowers', component: SuperpowerComponent},
  { path: "superpower-add", title: 'Add Superpowers', component: SuperpowerAddComponent},
  { path: "superpower-delete", title: 'Confirm Delte', component: SuperpowerDeleteComponent},
  { path: "superpower-update", title: 'Update Superpowers', component: SuperpowerUpdateComponent},
  { path: "categories", title: 'Categories', component: CategoryComponent},
  { path: "category-add", title: 'Add Categories', component: CategoryAddComponent},
  { path: "category-delete", title: 'Confirm Delte', component: CategoryDeleteComponent},
  { path: "category-update", title: 'Update Categories', component: CategoryUpdateComponent},

]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    HeaderComponent,
    SuperheroComponent,
    SuperheroAddComponent,
    SuperheroUpdateComponent,
    SuperheroDeleteComponent,
    SuperpowerComponent,
    SuperpowerAddComponent,
    SuperpowerUpdateComponent,
    SuperpowerDeleteComponent,
    SuperpowerDetailsComponent,
    SuperheroDetailsComponent,
    ApplicationComponent,
    CategoryComponent,
    CategoryAddComponent,
    CategoryDeleteComponent,
    CategoryUpdateComponent
  ],
  imports: [
    BrowserModule,
//    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
  ],
  providers: [SuperheroService, SuperpowerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
