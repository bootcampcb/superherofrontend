import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Superpower } from '../models/superpower.model';
import { SuperpowerService } from '../providers/superpower.service';

@Component({
  selector: 'app-superpower',
  templateUrl: './superpower.component.html',
  styleUrls: ['./superpower.component.css']
})
export class SuperpowerComponent {
  superpowers: Array<Superpower> = [];
  heroId!: number;
  constructor(private activatedRoute: ActivatedRoute, private SuperpowerService: SuperpowerService) { }

  ngOnInit(): void {
    this.SuperpowerService.getSuperpowers().subscribe((data) => {
      this.superpowers = data;
      console.dir(this.superpowers);
    });
  }
  onClickSearchButton() {
    this.SuperpowerService.getSuperpowersByHero(this.heroId).subscribe((data) => {
      this.superpowers = data;
    });
  }
}
